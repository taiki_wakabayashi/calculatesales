package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";

	//支店定義に関するエラー
	private static final String BRANCH_DEFINITION = "支店定義";
	private static final String BRANCH_REGEX = "^\\d{3}";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";

	//商品定義ファイルに関するエラー
	private static final String COMMODITY_DEFINITION = "商品定義";
	private static final String COMMODITY_REGEX = "^[A-Za-z0-9]{8}";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";

	//売上ファイルに関するエラー
	private static final String DEGITS_OVER = "合計金額が10桁を超えました";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません。";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数の確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コード、商品名、売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, BRANCH_REGEX, BRANCH_DEFINITION, branchNames, branchSales)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, COMMODITY_REGEX, COMMODITY_DEFINITION, commodityNames,
				commoditySales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		//売上ファイルの抽出・格納
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String fileNames = files[i].getName();

			//ファイルであり、かつファイル名がフォーマット通りか
			if (files[i].isFile() && fileNames.matches("^\\d{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルのファイル名が連番になっているかの確認
		Collections.sort(rcdFiles);
		for (int i = 0; i < (rcdFiles.size() - 1); i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int later = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((later - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		//売上ファイルから支店コード、商品コード、売上金額を抽出・格納
		BufferedReader br = null;
		String line;

		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				List<String> fileSalesItems = new ArrayList<>();
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				while ((line = br.readLine()) != null) {
					fileSalesItems.add(line);
				}

				//読み込んだファイルがフォーマット通り3行に分けているか
				if (fileSalesItems.size() != 3) {
					System.out.println(file.getName() + FILE_INVALID_FORMAT);
					return;
				}

				//売上ファイル内の支店コードのフォーマット確認
				if (!branchSales.containsKey(fileSalesItems.get(0))) {
					System.out.println(file.getName() + BRANCH_CODE_ERROR);
					return;
				}

				//売上ファイル内の商品コードのフォーマット確認
				if (!commoditySales.containsKey(fileSalesItems.get(1))) {
					System.out.println(file.getName() + COMMODITY_CODE_ERROR);
					return;
				}

				//売上ファイル内の売上金額が全て数字で表記されているか
				if (!fileSalesItems.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上ファイルの文字列を格納したリストの要素[2]をLong化
				Long fileSale = Long.parseLong(fileSalesItems.get(2));
				Long branchSaleAmount = branchSales.get(fileSalesItems.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(fileSalesItems.get(1)) + fileSale;

				//支店・商品それぞれで合計金額が10桁を超えたら処理終了
				if (branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(DEGITS_OVER);
					return;
				}

				//支店Mapに対応する値を追加
				branchSales.put(fileSalesItems.get(0), branchSaleAmount);
				commoditySales.put(fileSalesItems.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();

					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String regex, String fileType,
			Map<String, String> mapNames,
			Map<String, Long> mapSales) {
		BufferedReader br = null;

		//ファイルの存在確認
		File exitNameFile = new File(path, fileName);
		if (!(exitNameFile.exists())) {
			System.out.println(fileType + FILE_NOT_EXIST);
			return false;
		}

		try {
			File file = new File(path, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				//ファイルの読み込み
				String[] fileItems = line.split(",");

				//ファイルのフォーマット確認
				if (fileItems.length != 2 || !fileItems[0].matches(regex)) {
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;

				}
				mapNames.put(fileItems[0], fileItems[1]);

				//売上ファイルの支店コード、商品定義の所品コードをkeyで設定
				//ここで設定しておくことで、mapNamesのkeyと同じ内容＋文字で設定ができる
				mapSales.put(fileItems[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fr = new FileWriter(file);
			bw = new BufferedWriter(fr);

			for (String key : mapNames.keySet()) {
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();

				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
